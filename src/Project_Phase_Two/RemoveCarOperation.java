/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: DelOperation
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @description:Remove product from the cart 

 **/
public class RemoveCarOperation implements SellOperation {
    @Override
    public void work(ItemList itemList,Cart cart) {
        System.out.println("Delete from Cart");
        System.out.println("Input the Delete Item ID");

        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        List<Item> list = cart.getList();

        for (int i = 0; i < itemList.getSize(); i++) {
            Item obj = itemList.getItem(i);
            if (obj.getId() == id) {
                System.out.println("Current Item ID ："+obj.getId());
                System.out.println("Current Item Name ："+obj.getName());
                System.out.println("Current Item Price ："+obj.getPrice());
                System.out.println("Current Item Sold or Not："+obj);
                list.remove(obj);
                cart.setList(list);
                System.out.println("Delete Completed");
                return;
            }

        }
    }
}