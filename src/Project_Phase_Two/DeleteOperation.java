/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: DelOperation
 */

import java.util.Scanner;

/**
 *
 * @description:
 **/
public class DeleteOperation implements SellOperation{
    @Override
    public void work(ItemList itemList,Cart cart){
        System.out.println("Delete Item ");
        System.out.println("Input the Id of Delete Item: ");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        System.out.println("Input the Name of Delete Item: ");
        String name = scanner.next();
        int i = 0;
        int j = 0;
        
        if(id >= itemList.getSize()){
        	System.out.println("The Item Inputed [id:"+id+",name:"+name+"] can't found in Stock! Delete Failure!");
            return;
        }
        
        Item[] books = new Item[itemList.getSize()];
        
        for (; i < itemList.getSize(); i++) {
        	Item aa = itemList.getItem(i);
        	int id1 = aa.getId();
            if (id == itemList.getItem(i).getId()) {
            	if(!name.equals(itemList.getItem(i).getName())){
            		System.out.println("id and name cannot match");
            		return;
            	} 
            }else{
            	books[j] = itemList.getItem(i);
            	itemList.setItem(j, books[j]);
            	j++;
            }
        }
        
        /*if (i == itemList.getSize() - 1) {            
            itemList.setSize(itemList.getSize() - 1);
            System.out.println("Delete Successfully");
            return;
        }*/
        
        //itemList.setItem(i, itemList.getItem(itemList.getSize() - 1));
       
        itemList.setSize(itemList.getSize() - 1);
        System.out.println("Delete Completed");
    }
}