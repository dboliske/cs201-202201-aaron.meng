/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @Package: PACKAGE_NAME
 * @ClassName: AgeRestrictedItem
 * @Author: wujh 2535755228@qq.com
 * @Description: Age Sales Restriction Items
 * @Date: 2022-04-28 8:15
 */

/**
 *
 * @description: Age Sales Restriction Items
 * @author: wujh 2535755228@qq.com
 * @create: 2022-04-28 08:15
 **/
public class AgeRestrictedItem extends Item{

    private int ageRestricted;//Restriction Age

    public int getAgeRestricted() {
        return ageRestricted;
    }

    public AgeRestrictedItem(int ageRestricted) {
        this.ageRestricted = ageRestricted;
    }
    public AgeRestrictedItem() {
        super();
    }
    public void setAgeRestricted(int ageRestricted) {
        this.ageRestricted = ageRestricted;
    }

    @Override
    public String toString() {
        //return super.getId()+","+super.getName()+","+super.getPrice()+","+this.getAgeRestricted();
        if(super.getAge() != null){
        	return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice()+",ageRestricted:"+super.getAge();
        }else{
        	return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice()+",ageRestricted:"+this.getAgeRestricted();
        }
    }
}
