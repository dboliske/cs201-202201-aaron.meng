/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: IOperation
 */

/**
 *
 * @description:
 **/
public interface SellOperation {
    public void work (ItemList itemList,Cart cart );
}