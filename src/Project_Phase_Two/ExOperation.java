/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @Date: 2022-04-27 9:00
 */

/**
 *
 * @description:

 **/
public class ExOperation implements SellOperation {
    @Override
    public void work (ItemList itemList,Cart cart){
        System.out.println(" Thanks for Use, Quit Scuccessfully! ");
        // Exit method to close the current Java Progress.
        System.exit(0);
        //exit code "0", abnormal code "!0". 
    }
}