/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: SuperMarDemo
 */

import java.util.Scanner;

/**
 *
 * @description:

 **/
public class SuperMarDemo {
	
	private ItemList itemList = new ItemList();
	private Cart cart = new Cart();
	
    //Main Menu
    public int mainMenu() {
        Scanner sc = new Scanner(System.in);
        int choose;
        do {
            System.out.println("*******Welcome to the Store Operation System*******");
            System.out.println("\t1.Stock ");
            System.out.println("\t2.Sell ");
            System.out.println("\t3.Exit ");
            System.out.println("*******************************");
            System.out.print("Please input your choice 1 2 3：");
            choose = sc.nextInt();
            switch (choose) {
                case 1:
                    stockMar();
                    break;
                case 2:
                    goodsMar();
                    break;
            }
        } while (choose!=3);
        System.out.println("Exit Completed");
        return choose;
    }

    //Stock 
    public void stockMar() {
        Scanner sc = new Scanner(System.in);
        int choose;
        do {
            System.out.println("************ 1. Stock ************");
            System.out.println("\t1.Item list Item List ");
            System.out.println("\t2.Add item Adding Item ");
            System.out.println("\t3.Update item Modify Item ");
            System.out.println("\t4.Delete item Delete Item ");

            System.out.println("\t5.Read data from file ");
            System.out.println("\t6.Save data to file ");
            System.out.println("\t7.Back ");


            System.out.println("********************************");
            System.out.print(" Input Choice 1 - 7：");
            choose = sc.nextInt();
            
            switch (choose) {
                case 1:
                    System.out.println("Item List ");
                    int size = itemList.getSize();
                    for(int i=0;i<size;i++){
                    	System.out.println(itemList.getItem(i));
                    }
                    System.out.println("size======"+size);
                    break;
                case 2:
                    System.out.println("Add Item ");
                    AddItemOperation addItemOperation = new AddItemOperation();
                    addItemOperation.work(itemList, cart);
                    break;
                case 3:
                    System.out.println("Modify Item ");
                    EditOperation editOperation = new EditOperation();
                    editOperation.work(itemList, cart);
                    break;
                case 4:
                    System.out.println("Delete Item ");
                    DeleteOperation deleteOperation = new DeleteOperation();
                    deleteOperation.work(itemList, cart);
                    break;
                case 5:
                    System.out.println("Read Data from CSV File ");
                    
                    ReadCSVOperation readCSVOperation = new ReadCSVOperation();
                    readCSVOperation.work(itemList, cart);
                    size = itemList.getSize();
                    break;
                case 6:
                    System.out.println("Save Data to CSV File ");
                    SaveCSVOperation saveCSVOperation = new SaveCSVOperation();
                    saveCSVOperation.work(itemList, cart);
                    break;
            }
        } while (choose!=7);
    }


    //Stock System 
    public void goodsMar() {
        Scanner sc = new Scanner(System.in);
        int choose;
        do {
            System.out.println("************ 2. Sell ************");
            System.out.println("\t1.Search Item ");
            System.out.println("\t2.Add Item to the Cart ");
            System.out.println("\t3.Remove Item from the Cart ");
            System.out.println("\t4.Cart list ");
            System.out.println("\t5.Check out ");
            System.out.println("\t6.Return ");


            System.out.println("********************************");
            System.out.print("Please Input the Selection 1 - 6：");
            choose = sc.nextInt();
            switch (choose) {
                case 1:
                    System.out.println(" Search Item ");
                    FindOperation findOperation = new FindOperation();
                    findOperation.work(itemList, cart);
                    break;
                case 2:
                    System.out.println(" Add item into the Cart ");
                    AddToCarOperation addToCarOperation = new AddToCarOperation();
                    addToCarOperation.work(itemList, cart);
                    break;
                case 3:
                    System.out.println(" Remove from the Cart ");
                    RemoveCarOperation removeCarOperation = new RemoveCarOperation();
                    removeCarOperation.work(itemList, cart);
                    break;
                case 4:
                	System.out.println(" Cart list ");
                	FindCarOperation findCarOperation = new FindCarOperation();
                	findCarOperation.work(itemList, cart);
                	break;
                case 5:
                    System.out.println(" Check out ");
                    CheckOutOperation checkOutOperation = new CheckOutOperation();
                    checkOutOperation.work(itemList, cart);
                    break;
            }
        } while (choose!=6);
    }
    public static void main(String[] args) {
        SuperMarDemo smd = new SuperMarDemo();
        smd.mainMenu();
    }
}