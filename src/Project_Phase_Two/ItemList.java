/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: BookList
 */

/**
 *
 * @description:
 **/
public class ItemList {
    //Creat a Item List
    private Item[] books = new Item[10000];
    //define the capacity
    private int size = 0;

       public ItemList() {}
    
    public Item getItem(int index) {
        return books[index];
    }
    //define Item information
    public void setItem(int index, Item item) {
        books[index] = item;
    }


    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }


}
