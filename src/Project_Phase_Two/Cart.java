/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: Cart
 * @Description: Cart
 * 
 */

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @description: Cart
 **/
public class Cart {
    private  List<Item> list = new ArrayList<Item>();//Sale Item
    private  double allPrice;//Total Amount


    public Cart(List<Item> list) {
        this.list = list;
    }
    public Cart() {
    }
    @Override
    public String toString() {
        return "Cart{" +
                "list=" + list +
                ", allPrice=" + allPrice +
                '}';
    }

    public List<Item> getList() {
        return list;
    }

    public void setList(List<Item> list) {
        this.list = list;
    }

    public double getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(double allPrice) {
        this.allPrice = allPrice;
    }
}
