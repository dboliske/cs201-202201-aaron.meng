/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: FinOperation
 */

import java.util.Scanner;

/**
 *
 * @description:
 **/
public class FindOperation implements SellOperation{
    @Override
    public void work(ItemList itemList,Cart cart){
        System.out.println("Check Stock！");
        // Input Item for search
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input the Item Name: ");
        String name = scanner.next();
        
        for (int i = 0; i < itemList.getSize(); i++) {
            if (itemList.getItem(i).getName().contains(name)) {
                System.out.println(itemList.getItem(i));
            }
        }
    }
}