/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: ProduceItem
 * @Description: Produce Item
 
 */

import java.util.Date;

/**
 *
 * @description: Produce Item
 **/
public class ProduceItem extends Item{

    //private Date expDate;//Expired Date
	private String expDate;
    private Integer ageRestricted;

    public ProduceItem() {
        super();
    }

    @Override
    public String toString() {
    	if(super.getDate() != null)
    		return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice()+",expDate:"+super.getDate();
    	else if(super.getAge() != null)
    		return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice()+",ageRestricted:"+super.getAge();
    	else
    		return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice();
    }

    public String getExpDate() {
        return expDate;
    }
    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

	public Integer getAgeRestricted() {
		return ageRestricted;
	}
	public void setAgeRestricted(Integer ageRestricted) {
		this.ageRestricted = ageRestricted;
	}
    
    
}
