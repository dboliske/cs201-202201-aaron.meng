/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: DelOperation
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @description: Check Out
 **/
public class CheckOutOperation implements SellOperation {
    @Override
    public void work(ItemList itemList,Cart cart) {
        boolean flg = false;
        for (int i = 0; i < cart.getList().size(); i++) {
        	Item obj = cart.getList().get(i);
        	if(obj.getAge() != null && !obj.getAge().equals("")){
        		flg = true;
        		break;
        	}
        }
        
        int buyerAge = 0;
        if(flg){
        	System.out.println("Input the Age of Buyer");
            Scanner scanner = new Scanner(System.in);
            buyerAge = scanner.nextInt();
        }
        
        //System.out.println("Items In Cart：");
        
        for (int i = 0; i < cart.getList().size(); i++) {
            Item obj = cart.getList().get(i);
            if(obj.getAge() != null && !obj.getAge().equals("")){
            	int age = Integer.parseInt(obj.getAge());
            	if(buyerAge <= age){
            		System.out.println("id:"+obj.getId()+",name:"+obj.getName()+" do not meet the min age requirement "+age+",");
            		return;
            	}
            }else if(obj.getDate() != null && !obj.getDate().equals("")){
            	String date = obj.getDate();
            	String now = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
            	System.out.println("date==="+date+",now===="+now);
            	if(now.compareTo(date) >= 1){
            		System.out.println("id:"+obj.getId()+",name:"+obj.getName()+" exceed the shelf life "+date);
            		return;
            	}
            }
            
            System.out.println("Items In Cart："+obj.getName() + "," + obj.getPrice() + ","+obj.getId()+"");
        }
        
        System.out.println("check out: Yes/No");
        Scanner scanner2 = new Scanner(System.in);
        String flag = scanner2.next();
        if(flag.equals("Yes")){
        	System.out.println("check out successfully !");
        }else{
        	return;
        }
    }
}