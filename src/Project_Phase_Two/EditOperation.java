/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: DelOperation
 */

import java.util.Scanner;

/**
 *
 * @description:
 **/
public class EditOperation implements SellOperation {
    @Override
    public void work(ItemList itemList, Cart cart) {
        System.out.println("Mofify Item");
        System.out.println("Input the Modify Item ID");

        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();

        for (int i = 0; i < itemList.getSize(); i++) {

            //Item obj = itemList.getItem(i);
        	Item obj = itemList.getItem(i);

            if (obj.getId() == id) {
                System.out.println("Current Item ID："+obj.getId());
                System.out.println("Current Item Name ："+obj.getName());
                System.out.println("Current Item Price ："+obj.getPrice());
                System.out.println("Current Item Sold Out："+obj);


                //System.out.println("Input the New Item ID");
                //obj.setId(scanner.nextInt());
                System.out.println("Input the New Item Price");
                Double price = scanner.nextDouble();
                obj.setPrice(price);
                System.out.println("Input the New Item Name");
                obj.setName(scanner.next());
                
                if(obj.getDate() != null){
                	System.out.println("Input the Item ExpDate MM/DD/YYYY: ");
                	System.out.println("ExpDate:shelves valid date");
                	obj.setDate(scanner.next());
                }else if(obj.getAge() != null){
                	System.out.println("Input the Item Age: ");
                	System.out.println("Age:Restriction of the qualified buyer");
                	obj.setAge(scanner.next());
                }
                System.out.println("Modification Done");
                return;
            }

        }
    }
}