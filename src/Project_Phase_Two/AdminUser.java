/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: Admin
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @description:
 **/
public class AdminUser extends SysUser {
    public ItemList itemList;
    public Cart cart;

    /*
      private String name;
      //管理员能够支持的操作，这些操作存在于IOperation这个接口里面
      //我们可以定义一个IOperation[]数组,因为我们知道接口无法实现实例化
      //所以这个数组中放置的内容，就是实现了该接口类的实例。
      //这里也能解释为什么我们要把若干个操作提取出共同信息，创建一个IOperation接口
      //因为如果过不把这些操作类提取出共性搭建接口的话，在管理员类中就很难用过一个数组来描述管理员自身
      //都支持哪些操作。
      private IOperation[] operations;
      */
    public AdminUser(String name){
        this.name = name;
        this.operations = new SellOperation[] {
                new ReadCSVOperation(),
                new FindOperation(),
                new AddItemOperation(),
                new EditOperation(),
                new DeleteOperation(),
                new ShowOperation(),
                new SaveCSVOperation(),
                new ExOperation(),
                new AddToCarOperation(),
                new RemoveCarOperation(),
                new CheckOutOperation()

        };
        this.itemList = new ItemList();
        this.cart = new Cart();
        List<Item> list = new ArrayList<>();
        cart.setList(list);
    }
    @Override
    public void menu(String type) {
        int choice = 0;
        while (true) {
            System.out.println("=========================");
            System.out.println("Hello! " + name + ", Welcome to the Store Operation System");
            if (type.equals("1")) {
                System.out.println(" 1. Adding CSV File");
                System.out.println(" 2. Check the Item");
                System.out.println(" 3. Add the Item");
                System.out.println(" 4. MOdity the Item");
                System.out.println(" 5. Delete the Item");
                System.out.println(" 6. Print the Item List");
                System.out.println(" 7. Save data to CSV File");
                System.out.println(" 8. Return");
            } else {
                System.out.println("9.  Add product to the cart");
                System.out.println("10. Remove product from the cart");
                System.out.println("11. Check out");
                System.out.println("12. Return to the Menu");

            }
            System.out.println("=========================");
            System.out.println("Input Your Choice: ");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();
            if(type.equals(1)){
                if(choice < 0 && choice >8){
                    System.out.println("Please Input Again: ");
                    break;
                }
            }else if(type.equals(2)){
                if(choice < 9 && choice >12){
                    System.out.println("Please Input Agaiin: ");
                    break;
                }
            }
            switch (choice) {
                case 8:
                case 12:
                    setPmenu();
                    break;
            }
            doOperation(choice, itemList, cart);
        }
    }

    @Override
    public void setPmenu() {
        Scanner sc = new Scanner(System.in);
        int choose;
        System.out.println("*******Welcome to The Store Management System*******");
        System.out.println("\t1. Stock ");
        System.out.println("\t2. Sale ");
        System.out.println("\t3. Exit");
        System.out.println("*******************************");
        System.out.print("Please Choice 1 2 3 ");
        choose = sc.nextInt();
        switch (choose) {
            case 1:
                menu("1");
            case 2:
                menu("2");
        }
    }
}
