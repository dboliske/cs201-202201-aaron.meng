/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: DelOperation
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;

/**
 *
 * @description:

 **/
public class SaveCSVOperation implements SellOperation {
    @Override
    public void work(ItemList itemList,Cart cart) {
        System.out.println("Save Data to CSV File");
        
        File writeFile = new File("123\\stock.csv");
        try{
            
            BufferedWriter writeText =new BufferedWriter(new OutputStreamWriter(new FileOutputStream(writeFile),"GBK"));
           
            for(int i=0;i<itemList.getSize();i++){
               if(itemList.getItem(i).getDate() != null){
            	   writeText.write(itemList.getItem(i).getName()+","+itemList.getItem(i).getPrice()+","+itemList.getItem(i).getDate());
               }else if(itemList.getItem(i).getAge() != null){
            	   writeText.write(itemList.getItem(i).getName()+","+itemList.getItem(i).getPrice()+","+itemList.getItem(i).getAge());
               }else{
            	   writeText.write(itemList.getItem(i).getName()+","+itemList.getItem(i).getPrice());
               }
                //writeText.write(itemList.getItem(i).getId()+","+itemList.getItem(i).getName()+","+itemList.getItem(i).getPrice());
            	
                writeText.newLine();
              
            }
           
            writeText.flush();
            writeText.close();
        }catch (FileNotFoundException e){
            System.out.println("Failed to Find the File"+e.getMessage());
        }catch (IOException e){
            System.out.println("File Erro"+e.getMessage());
        }

    }
}