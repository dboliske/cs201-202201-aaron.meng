import java.util.List;

public class FindCarOperation implements SellOperation {

	@Override
	public void work(ItemList itemList, Cart cart) {
		List<Item> list = cart.getList();
		if(list != null){
			for (int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i));
			}
		}
	}

}
