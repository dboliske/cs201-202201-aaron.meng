/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: ShelvedItem
 */

/**
 *
 * @description: Shelved Item
 **/
public class ShelvedItem extends Item{
    public ShelvedItem() {
        super();
    }

    @Override
    public String toString() {
    	if(super.getDate() != null){
    		return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice()+",expDate:"+super.getDate();
    	}else if(super.getAge() != null){
    		return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice()+",ageRestricted:"+super.getAge();
    	}else{
    		return "id:"+super.getId()+",name:"+super.getName()+",price:"+super.getPrice();
    	}       
    }
}
