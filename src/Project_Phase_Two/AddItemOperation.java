/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @Package: PACKAGE_NAME
 * @ClassName: AddOperation
  */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 *
 * @description:
 *  **/
public class AddItemOperation implements SellOperation{
    @Override
    public void work(ItemList itemList,Cart cart){
        System.out.println("Adding New Item, please follow the prompt");
        Scanner scanner = new Scanner(System.in);

        //System.out.println("Input the new Item Number: ");
        //int id = scanner.nextInt();

        System.out.println("Input the New Item Name:");
        String name = scanner.next();



        System.out.println("Input the New Item Price:");
        double price = scanner.nextDouble();

        System.out.println("Input the Item type: Age or ExpDate MM/DD/YYYY or None: ");
        System.out.println("Age:Restriction of the qualified buyer");
        System.out.println("ExpDate:shelves valid date");
        System.out.println("None:for all other common goods");
        String type = scanner.next();

        int curSize = itemList.getSize();
        //Item newItem = new Item(curSize,name, price);
        ProduceItem newItem = new ProduceItem();
        newItem.setId(curSize);
        newItem.setName(name);
        newItem.setPrice(price);
        if(!type.equals("None")){
        	if(type.contains("/")){
        		newItem.setExpDate(type);
        		newItem.setDate(type);
        	}else{
        		newItem.setAge(type);
        		newItem.setAgeRestricted(Integer.parseInt(type));
        	}
        }
        itemList.setItem(curSize, newItem);
        
        itemList.setSize(curSize + 1);
        System.out.println("Adding Scuccessfully!");
    }
}