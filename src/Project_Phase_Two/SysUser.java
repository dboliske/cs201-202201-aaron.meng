/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: User
 */

/**
 *
 * @description:
 **/
abstract public class SysUser {
        protected String name;
        protected SellOperation[] operations;


        public abstract void menu(String type);

        public abstract void setPmenu();


    public void doOperation(int choice, ItemList itemList,Cart cart) {

            SellOperation operation = this.operations[choice - 1];
          
            operation.work(itemList,cart);
        }

    }
