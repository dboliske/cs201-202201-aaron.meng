/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: DelOperation
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @description:
 * @author: wujh 2535755228@qq.com
 * @create: 2022-04-27 09:01
 **/
public class ReadCSVOperation implements SellOperation {
    @Override
    public void work(ItemList itemList,Cart cart) {
        System.out.println("Read Items from CSV File");

        //try (BufferedReader br = Files.newBufferedReader(Paths.get("C:\\Users\\Administrator\\Desktop\\stock.csv"))) {
        //try (BufferedReader br = Files.newBufferedReader(Paths.get("D:\\stock.csv"))) {
        try{
        	File file = new File("123/stock.csv");
        	BufferedReader br = new BufferedReader(new FileReader(file));
            // CSV separation 
            String DELIMITER = ",";
            // Read by line
            String line;
            int num=0;
            while ((line = br.readLine()) != null) {

                // Split
                String[] columns = line.split(DELIMITER);

                // Print Line
                // Check length and type
                // pencil,1.29 Shelve Item
                // wine,12.99,21 Age Restriction Item
                // banana,0.62,04/29/2022 Produce Item
                if(columns.length ==3){
                  
                    if(columns[2].contains("/")){
                        ProduceItem item = new ProduceItem();
                        item.setName(columns[0]);
                        item.setPrice(Double.parseDouble(columns[1]));
                        item.setId(num);
                        item.setDate(columns[2]);
                        
                        //SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

                        //item.setExpDate(sdf.parse(columns[2]));
                        item.setExpDate(columns[2]);
                        itemList.setItem(itemList.getSize(),item);
                        itemList.setSize(itemList.getSize() + 1);
                        //System.out.println(itemList.getItem(itemList.getSize()-1));
                        //System.out.println(itemList.getSize());
                    }else{
                        AgeRestrictedItem item = new AgeRestrictedItem();
                        item.setName(columns[0]);
                        item.setPrice(Double.parseDouble(columns[1]));
                        item.setId(num);
                        item.setAgeRestricted(Integer.parseInt(columns[2]));
                        item.setAge(columns[2]);
                        itemList.setItem(itemList.getSize(),item);
                        itemList.setSize(itemList.getSize() + 1);
                        //System.out.println(itemList.getItem(itemList.getSize()-1));
                        //System.out.println(itemList.getSize());
                    }

                }else if(columns.length ==2){
                    ShelvedItem item = new ShelvedItem();
                    item.setName(columns[0]);
                    item.setPrice(Double.parseDouble(columns[1]));
                    item.setId(num);
                    itemList.setItem(itemList.getSize(),item);
                    itemList.setSize(itemList.getSize() + 1);
                    //System.out.println(itemList.getItem(itemList.getSize()-1));
                    //System.out.println(itemList.getSize());
                }
                num ++;
                //ending with date
                //System.out.println( String.join(", ", columns));
            }

        } catch (IOException ex) {
            System.out.println("File Erro, Please contact the Admin："+ex.getMessage());
            ex.printStackTrace();
        }

    }
}