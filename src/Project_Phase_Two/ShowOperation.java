/**
 * @Meng_Zhaoming_CS201_SE001_202201_Project
 * @ProjectName: StoreOperation
 * @ClassName: DisplayOperation
 */

/**
*
* @description:
**/
public class ShowOperation implements SellOperation{
    @Override
    public void work(ItemList itemList,Cart cart){
        for (int i = 0; i < itemList.getSize(); i++) {
                       System.out.println(itemList.getItem(i));

        }
    }
}
