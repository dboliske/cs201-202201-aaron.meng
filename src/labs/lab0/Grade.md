# Lab 0

## Total

18/20

## Break Down

* Eclipse "Hello World" program         5/5
* Correct TryVariables.java & run       4/4
* Name and Birthdate program            5/5
* Square
  * Pseudocode                          2/2
  * Correct output matches pseudocode   2/2
* Documentation                         0/2

## Comments

No documentation. Additionally, you should include the package declaration at the top of each file to make them executable in Eclipse. I didn't deduct any points for that, but something to keep in mind for the future.
