//Meng_zhaoming_CS201_SEC001_Lab005_CTAStation

package cs201_Lab005;

import java.util.Objects;

public class CTAStation extends GeoLocation {

    private String name;
    private String location;
    private boolean wheelchair;
    private boolean open;

    public CTAStation() {

    }

    public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
        super(lat, lng);
        this.name = name;
        this.location = location;
        this.wheelchair = wheelchair;
        this.open = open;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public boolean hasWheelchair() {
        return wheelchair;
    }

    public boolean isOpen() {
        return open;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWheelchair(boolean wheelchair) {
        this.wheelchair = wheelchair;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CTAStation that = (CTAStation) o;
        return wheelchair == that.wheelchair && open == that.open && Objects.equals(name, that.name) && Objects.equals(location, that.location);
    }

    @Override
    public String toString() {
        return "CTAStation{" +
                "name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", wheelchair=" + wheelchair +
                ", open=" + open +
                '}';
    }
}
