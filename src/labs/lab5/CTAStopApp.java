//Meng_Zhaoming_CS201_Sec001_Lab005_CTAStopApp

package cs201_Lab005;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;

public class CTAStopApp {
    public static void main(String[] args) {
        String fileName = "stations.csv";
        CTAStation[] stations = readFile(fileName);
        menu(stations);
    }

    public static CTAStation[] readFile(String fileName) {
        CsvReader reader = CsvUtil.getReader();
        CsvData data = reader.read(FileUtil.file(fileName));
        List<CsvRow> rows = data.getRows();
        return rows.stream().map(p -> new CTAStation(p.get(0), Double.parseDouble(p.get(1)), Double.parseDouble(p.get(2)),
                p.get(3), Boolean.parseBoolean(p.get(4)), Boolean.parseBoolean(p.get(5)))).toArray(CTAStation[]::new);
    }

    public static void menu(CTAStation[] stations) {
        String menu;
        do {
            System.out.println("1.Display Station Names");
            System.out.println("2.Display Stations with/without Wheelchair access");
            System.out.println("3.Display Nearest Station");
            System.out.println("4.Exit");
            Scanner input = new Scanner(System.in);
            menu = input.next();
            switch (menu) {
                case "1":
                    displayStationNames(stations);
                    break;
                case "2":
                    displayByWheelchair(input, stations);
                    break;
                case "3":
                    displayByNearest(input, stations);
                    break;
            }
        } while (!menu.equals("4"));
    }

    public static void displayStationNames(CTAStation[] stations) {
        Arrays.stream(stations).forEach(p -> System.out.println(p));
    }

    public static void displayByWheelchair(Scanner input, CTAStation[] stations) {
        String value;
        do {
            System.out.println("Enter y or n for accessibility:");
            value = input.next();
        } while (!value.equalsIgnoreCase("y") && !value.equalsIgnoreCase("n"));

        boolean wheelchair = value.equalsIgnoreCase("y");
        List<CTAStation> stationsByWheelchair = Arrays.stream(stations).filter(p -> p.hasWheelchair() == wheelchair).collect(Collectors.toList());
        if (stationsByWheelchair.size() == 0) {
            System.out.println("no stations are found");
        } else {
            stationsByWheelchair.forEach(p -> System.out.println(p));
        }
    }

    public static void displayByNearest(Scanner input, CTAStation[] stations) {
        System.out.println("Enter latitude:");
        String value = input.next();
        double lat = Double.parseDouble(value);
        System.out.println("Enter longitude:");
        value = input.next();
        double lng = Double.parseDouble(value);
        CTAStation nearestStation = Arrays.stream(stations).min(Comparator.comparing(p -> p.calcDistance(lat, lng))).get();
        System.out.println(nearestStation);
    }
}
