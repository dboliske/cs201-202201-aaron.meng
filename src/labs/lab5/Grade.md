# Lab 5

## Total

17/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 1/1
- Accessors:                    2/2
- Mutators:                     1/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                2/2
- Loops to display menu:        0/2
- Displays station names:       1/1
- Displays stations by access:  2/2
- Displays nearest station:     2/2
- Exits                         1/1

## Comments
- Methods both setLat and setLng didn't check the validation of lat and lng. -1
- The function of reading data required the outside package. 
  However the author didn't import that package in the Java Class, which leads to complier wrong. -2
- The author used many advanced features of Java to implement this assignment. It is pointless to learn the Java programming. 