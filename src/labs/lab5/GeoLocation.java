//Meng_Zhaoming_CS201_SEC001_Lab005_GeoLocation

package cs201_Lab005;

import java.util.Objects;

public class GeoLocation {

    private double lat;

    private double lng;

    public GeoLocation() {
    }

    public GeoLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(final double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(final double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", this.getLat(), this.getLng());
    }

    public boolean validLat(double lat) {
        return lat >= -90 && lat <= 90;
    }

    public boolean validLng(double lng) {
        return lng >= -180 && lng <= 180;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GeoLocation that = (GeoLocation) o;
        return Objects.equals(lat, that.lat) && Objects.equals(lng, that.lng);
    }

    public double calcDistance(GeoLocation g) {
        return this.calcDistance(g.getLat(), g.getLng());
    }

    public double calcDistance(double lat, double lng) {
        return Math.sqrt(Math.pow(this.getLat() - lat, 2) + Math.pow(this.getLng() - lng, 2));
    }
}
