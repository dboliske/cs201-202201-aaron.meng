//Meng_Zhaoming_CS201_SEC001_Lab006_DeliCounterApp

package cs201_lab006;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounterApp {

    public static ArrayList<String> customers = new ArrayList<>();

    public static void main(String[] args) {
        String menu;
        do {
            System.out.println("1.Add customer to queue");
            System.out.println("2.Help customer");
            System.out.println("3.Exit");
            Scanner input = new Scanner(System.in);
            menu = input.next();
            switch (menu) {
                case "1":
                    addCustomerToQuere(input);
                    break;
                case "2":
                    helpCustomer();
                    break;
            }
        } while (!menu.equals("3"));
    }

    public static void addCustomerToQuere(Scanner input) {
        System.out.println("enter customer name:");
        String customer = input.next();
        customers.add(customer);
    }

    public static void helpCustomer() {
        if (customers.size() == 0) {
            System.out.println("no customers are found.");
            return;
        }
        String customer = customers.get(0);
        System.out.println("helped customer:" + customer);
        customers.remove(customer);
    }
}
