//

package cs201_Lab004;

import java.util.Objects;

public class Potion {

	   
		    private String name;
		   
		    private Double strength;

		    public Potion() {

		    }

		  
		    public Potion(String name, Double strength) {
		        this.name = name;
		        this.strength = strength;
		    }

		   
		    public String getName() {
		        return name;
		    }

		    public void setName(String name) {
		        this.name = name;
		    }

		    public Double getStrength() {
		        return strength;
		    }

		    public void setStrength(Double strength) {
		        this.strength = strength;
		    }

		  
		    
		    public String toString() {
		        return "Potion{" +
		                "name='" + name + '\'' +
		                ", strength=" + strength +
		                '}';
		    }

		    private static final Double MIN_STRENGTH = 0.0;

		    private static final Double MAX_STRENGTH = 10.0;

		  
		    public boolean validStrength() {
		        return Double.compare(this.strength,MAX_STRENGTH) < 0 && Double.compare(this.strength,MIN_STRENGTH) > 0;
		    }

		  
		    public boolean equals(Object o) {
		        if (this == o) return true;
		        if (o == null || getClass() != o.getClass()) return false;
		        Potion potion = (Potion) o;
		        return Objects.equals(name, potion.name) && Objects.equals(strength, potion.strength);
		    }

		   
		    public int hashCode() {
		        return Objects.hash(name, strength);
		    }

		   
		    static final class TestPotion {
		        public static void main(String[] args) {
		           
		            final Potion defaultConstructor = new Potion();
		            defaultConstructor.setName("antidote");
		            defaultConstructor.setStrength(10.0);
		            System.out.println(defaultConstructor);

		          
		            final Potion nonDefaultConstructor = new Potion("poison",12.0);
		           
		            System.out.println(nonDefaultConstructor);

		       
		    }

	
	}

}
