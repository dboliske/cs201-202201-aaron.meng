package cs201_Lab004;


import java.util.Objects;

public class PhoneNumber{
	
		    
		    private String country;

		   
		    private String areaCode;

		   
		    private String number;


		   
		    public PhoneNumber() {

		    }

		   
		    public PhoneNumber(String country, String areaCode, String number) {
		        this.country = country;
		        this.areaCode = areaCode;
		        this.number = number;
		    }
	   


		    public String getCountry() {
		        return country;
		    }

		    public void setCountry(String country) {
		        this.country = country;
		    }

		    public String getAreaCode() {
		        return areaCode;
		    }

		    public void setAreaCode(String areaCode) {
		        this.areaCode = areaCode;
		    }

		    public String getNumber() {
		        return number;
		    }

		    public void setNumber(String number) {
		        this.number = number;
		    }

		    @Override
		    public String toString() {
		        return "PhoneNumber{" +
		                "number='" + number + '\'' +
		                '}';
		    }

		    private static final int AREA_CODE_LENGTH = 3;

		   
		    public boolean validAreaCodeLength() {
		        return validLength(this.areaCode,AREA_CODE_LENGTH);
		    }


		    private static final int NUMBER_LENGTH = 7;

		   
		    public boolean validNumberLength() {
		        return validLength(this.number,NUMBER_LENGTH);
		    }

		    private boolean validLength(String param,int length) {
		        return param != null && param.length() == length;
		    }

		  
		    public boolean equals(Object o) {
		        if (this == o) return true;
		        if (o == null || getClass() != o.getClass()) return false;
		        PhoneNumber that = (PhoneNumber) o;
		        return Objects.equals(number, that.number) && Objects.equals(country,that.country) && Objects.equals(areaCode,that.areaCode);
		    }

		   
		    public int hashCode() {
		        return Objects.hash(number);
		    }

		   
		    static final class TestPhoneNumber{
		        public static void main(String[] args) {
		           
		            final PhoneNumber defaultConstructor = new PhoneNumber();
		            defaultConstructor.setNumber("9000-0000");
		            defaultConstructor.setAreaCode("00001");
		            defaultConstructor.setCountry("China/Beijing");
		            System.out.println(defaultConstructor);

		           
		            final PhoneNumber nonDefaultConstructor = new PhoneNumber("China/Beijing","00001","9000-0000");
		            System.out.println(nonDefaultConstructor);

		        }
		    }
		
	}


