//Meng_Zhaoming_CS201_SEC001_Lab004_01

package cs201_Lab004;

import java.util.Objects;

public class GeoLocation { //Lab004 GeoLoation; 

		    private static final Double COMPARE_VALUE_90 = 90.0;
		    private static final Double COMPARE_VALUE_NEGATIVE_90 = -90.0;
		    private static final Double COMPARE_VALUE_180 = 180.0;
		    private static final Double COMPARE_VALUE_NEGATIVE_180 = -180.0;
		 
		    private Double lat;
		   
		    private Double lng;

		
		    public GeoLocation() {

		    }

		   
		    public GeoLocation(final Double lat, final Double lng) {
		        this.lat = lat;
		        this.lng = lng;
		    }

		  
		    public Double getLat() {
		        return lat;
		    }

		  
		    public void setLat(final Double lat) {
		        this.lat = lat;
		    }

		  
		    public Double getLng() {
		        return lng;
		    }

	
		    public void setLng(final Double lng) {
		        this.lng = lng;
		    }

		 
		    public String toString() {
		        return String.format("(lat:%s,lng:%s)", getLat(), getLng());
		    }

		  
		    public boolean isLatIn90() {
		        return compareLt(this.lat, COMPARE_VALUE_90) && compareLt(this.lat, COMPARE_VALUE_NEGATIVE_90);
		    }

		    
		    public boolean isLatIn180() {
		        return compareLt(this.lat, COMPARE_VALUE_180) && compareGt(this.lat, COMPARE_VALUE_NEGATIVE_180);
		    }

		    private boolean compareLt(Double var0, Double var1) {
		        return Double.compare(var0, var1) < 0;
		    }

		    private boolean compareGt(Double var0, Double var1) {
		        return Double.compare(var0, var1) > 0;
		    }

		 
		
		    public boolean equals(Object o) {
		        if (this == o) return true;
		        if (o == null || getClass() != o.getClass()) return false;
		        GeoLocation that = (GeoLocation) o;
		        return Objects.equals(lat, that.lat) && Objects.equals(lng, that.lng);
		    }

		    
		    public int hashCode() {
		        return Objects.hash(lat, lng);
		    }

		    static final class TestGeoLocation {
		        public static void main(String[] args) {
		            // default constructor
		            GeoLocation g1 = new GeoLocation();
		            g1.setLat(22.0);
		            g1.setLng(23.1);
		            // non-default constructor
		            GeoLocation g2 = new GeoLocation(23.0,24.1);
		            // result
		            System.out.printf("g1.lat: %s,g1.lng: %s,g2.lat: %s,g2.lng: %s,%n",g1.getLat(),g1.getLng(),g2.getLat(),g2.getLng());
		        }
		    }
		

		
		
	}


