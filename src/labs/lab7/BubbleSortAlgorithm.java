//Meng_Zhaoming_CS201_SE001_Lab007_BubbleSort

import java.util.Arrays;

public class BubbleSortAlgorithm {

    public static void main(String[] args) {
        int[] arr = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
        System.out.println("Before bubble sort:");
        System.out.println(Arrays.toString(arr));
        BubbleSortAlgorithm.bubbleSort(arr);
        System.out.println("After bubble sort:");
        System.out.println(Arrays.toString(arr));
    }

    public static void bubbleSort(int[] arr) {
        for (int i = arr.length - 1; i >= 1; i--) {
            for (int j = 0; j <= i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int t = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = t;
                }
            }
        }
    }
}