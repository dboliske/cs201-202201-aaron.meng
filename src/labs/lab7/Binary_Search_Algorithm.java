//Meng_Zhaoming_CS201_SEC001_Lab007_BinarySearch

public class Binary_Search_Algorithm {

    public static void main(String ar[]){
        String str[] ={"c", "html", "java", "python", "ruby", "scala"};
        String search= "python";
        int length = str.length-1;
        int i = Binary_Search_Algorithm.binarySearch(str, 0, length, search);
        System.out.println(i);
    }

    public static int binarySearch(String first[], int start, int end, String searchString){
        int mid = start + (end-start)/2;

        if(first[mid].compareTo(searchString)==0){
            return mid;
        }
        if(first[mid].compareTo(searchString)> 0){
            return binarySearch(first, start, mid-1, searchString);
        }else if(first[mid].compareTo(searchString)< 0){
            return binarySearch(first, mid+1, end, searchString);
        }
        return -1;
    }
}