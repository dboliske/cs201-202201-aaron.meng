//Meng_Zhaoming_CS201_SE001_Lab007_Selection_Sort_Algorithm

import java.util.Arrays;

public class Selection_Sort_Algorithm {

    public static void main(String a[]) {
        double[] arr = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        System.out.println("Before selection sort");
        System.out.println(Arrays.toString(arr));
        selectionSort(arr);
        System.out.println("After Selection Sort");
        System.out.println(Arrays.toString(arr));
    }

    public static void selectionSort(double[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[index]) {
                    index = j;
                }
            }
            double smallerNumber = arr[index];
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
    }
}