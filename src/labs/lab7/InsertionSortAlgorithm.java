//Meng_Zhaoming_CS201_SEC001_Lab007_InsertionSort


import java.util.Arrays;

public class InsertionSortAlgorithm {

    public static void main(String[] args) {
        String[] arr = {"cat", "fat", "dog", "apple", "bat", "egg"};
        System.out.println("Before insertion sort:");
        System.out.println(Arrays.toString(arr));
        InsertionSortAlgorithm.insertionSort(arr);
        System.out.println("After insertion sort:");
        System.out.println(Arrays.toString(arr));
    }

    public static void insertionSort(String[] arr) {
        int i, j;
        String key;
        for (j = 1; j < arr.length; j++) {
            key = arr[j];
            i = j - 1;
            while (i >= 0) {
                if (key.compareTo(arr[i]) > 0) {
                    break;
                }
                arr[i + 1] = arr[i];
                i--;
            }
            arr[i + 1] = key;
        }
    }
}

