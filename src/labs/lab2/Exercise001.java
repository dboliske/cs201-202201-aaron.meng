//Mengzhaoming_CS201_202201_Lab02_Exercise001

import java.util.Scanner;


public class Exercise001 { //generate the square with stars
	     
	     
	public static void main(String[] args) {
	    	
		var input = new Scanner(System.in); //create a scanner    
		
			
		  System.out.print("Please enter the Size number of the Square : ");//prompt for input
		
		
		    int N = input.nextInt(); //define the square size
	         
			for (int i = 1; i <= N; i++) {
	        	 
	             for (int j = 1; j <= N; j++) {
	            	 
	                System.out.print("* ");
	                
	             }
	            
	          System.out.println();
	             
			}
			
		
	}
	            

}
			
	    
