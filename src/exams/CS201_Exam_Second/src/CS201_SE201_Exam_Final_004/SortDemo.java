//Meng_Zhaoming_CS201_SE001_202201_Final_005

package CS201_SE201_Exam_Final_004;

public class SortDemo {

	public static void main(String[] args) {
		String[] arr = new String[]{"speaker","poem","passenger","tale","reflection","leader","quality","percentage","height","wealth","resource","lake","importance"};
		selectSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
	}
	
	public static void selectSort(String[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i; 
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j].compareTo(arr[minIndex]) < 0) {
                    minIndex = j; 
                }
            }
            
            if (i != minIndex) {
                String temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
           
        }
    }
}