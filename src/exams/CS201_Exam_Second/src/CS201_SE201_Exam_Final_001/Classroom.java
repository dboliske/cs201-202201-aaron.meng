//Meng_Zhaoming_CS201_SE001_202201_Final_005

package CS201_SE201_Exam_Final_001;

public class Classroom {

	String building;
	String roomNumber;
	int seats = 100;
	
	public Classroom() {
		super();
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	
	@Override
	public String toString() {
		return "Classroom [building=" + building + ", roomNumber=" + roomNumber + ", seats=" + seats + "]";
	}
		
}