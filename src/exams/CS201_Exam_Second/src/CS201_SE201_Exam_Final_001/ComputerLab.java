package CS201_SE201_Exam_Final_001;

public class ComputerLab extends Classroom {

	boolean computers;

	public ComputerLab() {
		super();
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}

	public boolean hasComputers(){
		return computers;
	}
	
	@Override
	public String toString() {
		return "ComputerLab [computers=" + computers + "]";
	}
	
}