# Final Exam

## Total

86/100

## Break Down

1. Inheritance/Polymorphism:    16/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                2/5
    - Methods:                  4/5
2. Abstract Classes:            16/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                3/5
    - Methods:                  3/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        15/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  5/5

## Comments

1. 	attributes building, roomNumber and seats should be protected. -3
	didn't check the validation of seats. -1
	
2.	attributes should be private. -2
	didn't check validation of radius, width and height. -2
	
3.	Error when the first input is 'Done' -1.
4.	ok
5.  Didn't implement jump search algorithm with recursively. -5 
