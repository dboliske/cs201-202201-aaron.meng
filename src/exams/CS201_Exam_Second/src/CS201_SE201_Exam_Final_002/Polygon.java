//Meng_Zhaoming_CS201_SE001_202201_Final_002

package CS201_SE201_Exam_Final_002;

public abstract class Polygon {

	String name;

	public Polygon() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Polygon [name=" + name + "]";
	}
	
	public abstract double area();
	
	public abstract double perimeter();
}