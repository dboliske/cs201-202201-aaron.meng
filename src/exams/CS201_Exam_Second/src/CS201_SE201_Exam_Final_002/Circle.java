//Meng_Zhaoming_CS201_SE001_202201_Final_002

package CS201_SE201_Exam_Final_002;

public class Circle extends Polygon {

	double radius = 1;
	
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	public Circle() {
		super();
	}

	@Override
	public double area() {
		return Math.PI*radius*radius;
	}

	@Override
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}

}