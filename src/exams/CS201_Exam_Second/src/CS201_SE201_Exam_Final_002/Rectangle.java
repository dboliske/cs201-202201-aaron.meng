package CS201_SE201_Exam_Final_002;

public class Rectangle extends Polygon {

	double width = 1;
	double height = 1;
	
	public Rectangle() {
		super();
	}
	
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", height=" + height + "]";
	}

	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimeter() {
		return 2.0 * (height + width);
	}

}